var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
    return 'id: ' + this.id + ' color: ' + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.allModels = [{
    name: 'montaña'
    },{
        name: 'calle'
    },{
        name: 'bicker'
    },{
        name: 'ruta'
    },
];

Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId) {
    debugger;
    var aBici = Bicicleta.allBicis.find(x => x.id === aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error('No existe la bicicleta');
}

Bicicleta.remove = function(aBiciId) {
    debugger;
    var aBici = Bicicleta.findById(aBiciId);
    Bicicleta.allBicis = Bicicleta.allBicis.filter(item => item.id !== aBici.id);
}


var a = new Bicicleta(1,'rojo','urbana',[-34.459510, -58.934238]);
var b = new Bicicleta(2,'rojo','urbana',[-34.459110, -58.934238]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;