var map = L.map('main_map').setView([-34.459320, -58.914588], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([-34.459310, -58.934238]).addTo(map);
L.marker([-34.459310, -58.924328]).addTo(map);
L.marker([-34.459310, -58.954118]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: (res)=>{
        console.log(res);
        res.bicicletas.forEach(item =>{
            L.marker(item.ubicacion,{title:item.id}).addTo(map);
        })
    }
})